<?php
$array = array();

for($i = 0; $i <= 100; $i++){

	$array[] = $i;
}

shuffle($array);


class sorting {


	public function quicksort(array $arr){

		$length = count($arr);

		if($length <= 1){

			return $arr;

		}

		$low = $high = array();
		$pivot = $arr[0];

		for($i = 1; $i < $length; $i++){

			if($arr[$i] < $pivot){

				$low[] = $arr[$i];

			} else {

				$high[] = $arr[$i];
			}
		}

		$result = array_merge($this->quicksort($low), array($pivot), $this->quicksort($high));

		return $result;
	}
}

$sorting = new sorting;
$sorted = $sorting->quicksort($array);

echo '<pre>';
	var_dump($sorted);
echo '</pre>';