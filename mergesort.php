<?php 

$array = array();

for($i = 0; $i <= 100; $i++){

	$array[] = $i;
}

shuffle($array);

class sorting {

	public function divide(array $arr){

		if(count($arr) <= 1){

			return $arr;
		}

		$left = $right = array();
		$middle = round(count($arr) / 2);

		for($i = 0; $i < $middle; $i++){

			$left[] = $arr[$i];

		}

		for($i = $middle; $i < count($arr); $i++){

			$right[] = $arr[$i];
		}

		$left = $this->divide($left);
		$right = $this->divide($right);

		$result = $this->conquer($left, $right);


		return $result;
	}

	public function conquer(array $left, array $right){


		$result = array();

		while(count($left) > 0 || count($right) > 0){

			$firstLeft = current($left);
			$firstRight = current($right);

			if(count($left) > 0 && count($right) > 0){


				if($firstLeft <= $firstRight){

					$result[] = array_shift($left);

				} else {

					$result[] = array_shift($right);
				}


			} else if(count($left) > 0){

				$result[] = array_shift($left);

			} else if(count($right) > 0){

				$result[] = array_shift($right);

			}
		}

		return $result;
	}
}


$sorting = new sorting;
$sorted= $sorting->divide($array);

echo '<pre>';
	var_dump($sorted);
echo '</pre>';